# STAY

**STAY** is an [acronym](http://acronymify.com/STAY?q=Spacewax+Content+Management+System) for 

(**S**)pacewax con(**T**)ent m(**A**)nagement s(**Y**)stem



## Purpose

A CMS built from scratch. Starting from the beginning and working ourway through the system, will give us keen aspects of the system.

We also get the chance to make it *do what we want it too do*.


### Planed Requirements
* PHP 7.0+
* MySQLi
* Webserver


### Planned Features

* Multi Language (i18n)
* Admin Area
* User Administration
* Module/Widget/Pluggin System
* Theming System


### Header details

All pages will contain header information as shown below.

```php
<?php
/**
*
* @copyright    Spacewax Technologies - OSSD
* @url          http://stay.spacewax.net/ The STAY Project
* @license      https://opensource.org/licenses/MIT MIT License (MIT)
* @license      see LICENSE
* @package      core
* @since        1.0
* @author       hyperclock <hyperclock@spacewax.net>
* @version      1.0.x-dev
*
*/
```
